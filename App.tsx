import React from 'react';
import SnapViewScreen from './src/screens/SnapViewScreen';
import {Provider} from 'react-redux';
import store from './src/store/configureStore';

const appWrappedRedux = () => (
    <Provider store={store}>
        <SnapViewScreen/>
    </Provider>
);

export default function App() {
    return appWrappedRedux();
}
