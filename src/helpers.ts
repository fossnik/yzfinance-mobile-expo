import {MetaCoin} from './store/coin/types';
import _ from 'lodash';

export const truncateNames = (coinList: MetaCoin[]) =>
    _.map(coinList, (coin: MetaCoin) => {
        coin.name = coin.name.match(/(.*)? USD$/)[1];
        return coin;
    });
