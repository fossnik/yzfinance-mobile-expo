import React from 'react';
import {Text, View} from 'react-native';
import Colors from './common/Colors';

export const Footer = (): JSX.Element => (
    <View style={{
        paddingVertical: 8,
    }}>
        <Text style={{
            textAlign: 'center',
            color: Colors.black,
        }}> Zack Hartmann {'\u00A9'} {new Date().getFullYear()}</Text>
    </View>
);
