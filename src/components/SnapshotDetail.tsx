import React from 'react';
import _ from 'lodash';
import {
    Button,
    Image,
    ScrollView,
    StyleSheet,
    Text,
    TextStyle,
    View,
    ViewStyle,
} from 'react-native';
import {Col, Grid, Row} from 'react-native-easy-grid';
import Colors from './common/Colors';
import {SnapshotSelectDate} from './SnapshotSelectDate';

import {CoinSnapshot} from '../store/snapshot/types';
import {MetaCoin} from '../store/coin/types';

export function SnapshotDetail(props: {
    onSelectSnap: (snap: number) => void,
    icons: {},
    coinSnapshots: CoinSnapshot[],
    selectedSnapNum: number,
    selectedCoin: MetaCoin,
}): JSX.Element {
    const {name, symbol_full, symbol_safe} = props.selectedCoin;

    const icon = <Image
        source={props.icons[symbol_safe]}
        style={{width: 60, height: 60}}
    />;

    const prevSnap: number | null = _.find(props.coinSnapshots, (snap: CoinSnapshot) =>
        snap.ID === props.selectedSnapNum - 1) ? (props.selectedSnapNum - 1) : null;

    const nextSnap: number | null = _.find(props.coinSnapshots, (snap: CoinSnapshot) =>
        snap.ID === props.selectedSnapNum + 1) ? (props.selectedSnapNum + 1) : null;

    const snapData: CoinSnapshot = _.find(props.coinSnapshots,
        (snap: CoinSnapshot) => snap.ID === props.selectedSnapNum);

    const {
        price,
        change,
        pChange,
        marketCap,
        volume,
        volume24h,
        totalVolume24h,
        circulatingSupply,
    } = _.mapValues(snapData, prop => prop.toLocaleString());

    return <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View style={{
            backgroundColor: Colors.light,
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
            paddingVertical: 20,
        }}>
            {icon}
            <View style={{
                flexDirection: 'column',
                maxWidth: 250,
            }}>
                <Text
                    numberOfLines={3}
                    style={{
                        textAlign: 'center',
                        fontSize: 32,
                    }}>{name}</Text>
                <Text style={{
                    fontSize: 22,
                    textAlign: 'center',
                    color: 'red',
                }}>{symbol_full}</Text>
            </View>
            {icon}
        </View>
        <View style={{
            backgroundColor: Colors.lighter,
            justifyContent: 'space-around',
            alignItems: 'center',
            flexDirection: 'row',
            paddingVertical: 20,
        }}>
            <View style={styles.navButton}>
                <Button
                    title='Previous'
                    onPress={() => props.onSelectSnap(props.selectedSnapNum - 1)}
                    disabled={!prevSnap}
                />
            </View>
            <View style={{
                flex: 3,
                flexDirection: 'column',
                alignItems: 'center',
            }}>
                <Text style={{fontSize: 20}}>{`Snap #${props.selectedSnapNum}`}</Text>
                <SnapshotSelectDate
                    onSelectSnap={(snap: number) => props.onSelectSnap(snap)}
                    selectedSnapNum={props.selectedSnapNum}
                    snapshots={props.coinSnapshots}
                />
            </View>
            <View style={styles.navButton}>
                <Button
                    title='Next'
                    onPress={() => props.onSelectSnap(props.selectedSnapNum + 1)}
                    disabled={!nextSnap}
                />
            </View>
        </View>
        <Grid style={{
            backgroundColor: Colors.minor,
            flexDirection: 'column',
            justifyContent: 'space-around',
        }}>
            <Row>
                <Col><Text style={styles.columnTitle}>Price (USD)</Text></Col>
                <Col><Text style={styles.columnData}>$ {price}</Text></Col>
            </Row>
            <Row>
                <Col><Text style={styles.columnTitle}>24 Hour Change</Text></Col>
                <Col>{snapData.change > 0 ?
                    <Text style={[styles.columnData, {color: 'green'}]}>
                        {`${change} ▲ ${pChange}%`}
                    </Text>
                    : snapData.change < 0 ?
                        <Text style={[styles.columnData, {color: 'red'}]}>
                            {`${change} ▼ ${pChange}%`}
                        </Text>
                        : <Text style={styles.columnData}>0</Text>
                }</Col>
            </Row>
            <Row>
                <Col><Text style={styles.columnTitle}>Market Cap</Text></Col>
                <Col><Text style={styles.columnData}>{marketCap}</Text></Col>
            </Row>
            <Row>
                <Col><Text style={styles.columnTitle}>Volume</Text></Col>
                <Col><Text style={styles.columnData}>{volume}</Text></Col>
            </Row>
            <Row>
                <Col><Text style={styles.columnTitle}>24 Hour Volume</Text></Col>
                <Col><Text style={styles.columnData}>{volume24h}</Text></Col>
            </Row>
            <Row>
                <Col><Text style={styles.columnTitle}>24 Hour Total Volume</Text></Col>
                <Col><Text style={styles.columnData}>{totalVolume24h}</Text></Col>
            </Row>
            <Row>
                <Col><Text style={styles.columnTitle}>Circulating Supply</Text></Col>
                <Col><Text style={styles.columnData}>{circulatingSupply}</Text></Col>
            </Row>
        </Grid>
    </ScrollView>;
}

const styles = StyleSheet.create<{
    navButton: ViewStyle;
    columnTitle: TextStyle;
    columnData: TextStyle;
}>({
    navButton: {
        margin: 8,
        flex: 2,
    },
    columnTitle: {
        textAlign: 'right',
        margin: 6,
        fontWeight: 'bold',
    },
    columnData: {
        textAlign: 'left',
        margin: 6,
    },
});
