import React from 'react';
import DatePicker from 'react-native-datepicker';
import _ from 'lodash';

import {CoinSnapshot} from '../store/snapshot/types';

const getDate = (dateTime: string) : string => dateTime.split(' ')[0];

export const SnapshotSelectDate = (props: {
    onSelectSnap: (snap: number) => void,
    snapshots: CoinSnapshot[],
    selectedSnapNum: number,
}): JSX.Element => {
    const onDateChange = (date: string) => {
        const snapMatch: CoinSnapshot | undefined = _.find(props.snapshots,
			(snap: CoinSnapshot) => snap.dateCreated.startsWith(date));

        if (snapMatch) {
            props.onSelectSnap(snapMatch.ID);
        }
    };

    const snapMatch: CoinSnapshot = _.find(props.snapshots,
        (snap: CoinSnapshot) => snap.ID === props.selectedSnapNum);

    const selectedDate: string = snapMatch ? getDate(snapMatch.dateCreated) : null;

    const minDate: string = getDate(props.snapshots[0].dateCreated);
    const maxDate: string = getDate(props.snapshots[props.snapshots.length - 1].dateCreated);

    return selectedDate && minDate && maxDate &&
		<DatePicker
			style={{
				width: 120,
				height: 40,
			}}
			date={selectedDate}
			mode="date"
			format="YYYY-MM-DD"
			minDate={minDate}
			maxDate={maxDate}
			confirmBtnText="Confirm"
			cancelBtnText="Cancel"
			customStyles={{
				dateIcon: {
					width: 30,
					height: 20,
				},
				dateInput: {
					width: 40,
					height: 20,
				},
			}}
			onDateChange={(date: string) => onDateChange(date)}
		/>;
};
