import React from 'react';
import {Dimensions, ImageBackground, PixelRatio, Text} from 'react-native';
import Colors from './common/Colors';

export const Header = (): JSX.Element => {
    const isSmallScreen = PixelRatio.getPixelSizeForLayoutSize(Dimensions.get('window').width) < 1080;
    const style = isSmallScreen ? {
        padding: 14,
    } : {
        paddingBottom: 6,
        paddingTop: 24,
        marginTop: 24,
        marginBottom: 8,
        paddingHorizontal: 38,
    };

    return (
        <ImageBackground
            accessibilityRole={'image'}
            source={require('../img/stock-ticker.jpg')}
            style={style}
            imageStyle={{
                opacity: 0.3,
                overflow: 'visible',
                resizeMode: 'cover',
                marginBottom: -96,
            }}>
            {
                 !isSmallScreen && <Text style={{
                    padding: 12,
                    fontSize: 34,
                    fontWeight: '900',
                    textAlign: 'center',
                    color: Colors.black,
                    backgroundColor: Colors.accent,
                    borderRadius: 12,
                }}>yZ Finance Mobile</Text>
            }
        </ImageBackground>
    );
};
