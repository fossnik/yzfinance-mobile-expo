import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import {
    SafeAreaView,
    StatusBar,
    Text
} from 'react-native';

import {
    Header,
    Footer,
    CoinMenu,
    SnapshotDetail,
    LoadingSpinner,
    icons,
} from '../components';

import {
    getListOfAllCoins,
    selectCoin
} from '../store/coin/coinActions';
import {
    getAllSnapsForCoin,
    selectSnap,
} from '../store/snapshot/snapActions';

import {AppState} from '../store';
import {CoinSnapshot, SnapAction, SnapIndex} from '../store/snapshot/types';
import {CoinAction, MetaCoin} from '../store/coin/types';

class SnapViewScreen extends PureComponent<{
    snapIndex: SnapIndex,
    coinList: MetaCoin[],
    selectedCoin: MetaCoin,
    selectedSnapNum: number,
    getListOfAllCoins: () => Promise<CoinAction>,
    getAllSnapsForCoin: (coin: string) => Promise<SnapAction>,
    selectCoin: (coin: MetaCoin) => Promise<CoinAction> | any,
    selectSnap: (snap: number) => Promise<SnapAction> | any,
}> {
    componentDidMount(): void {
        if (_.isEmpty(this.props.coinList)) {
            this.props.getListOfAllCoins()
                .catch(err => console.error({'Error Loading Coin Index': err}));
        }
        if (!this.props.snapIndex.hasOwnProperty(this.props.selectedCoin.symbol_safe)) {
            this.props.getAllSnapsForCoin(this.props.selectedCoin.symbol_safe)
                .catch(err => console.error({'Error Loading Snapshot': err}));
        }
    }

    onSelectCoin = (coin: MetaCoin): void => {
        if (!this.props.snapIndex[coin.symbol_safe]) {
            this.props.getAllSnapsForCoin(coin.symbol_safe)
                .then(()=>this.props.selectCoin(coin));
        } else {
            this.props.selectCoin(coin);
        }
    };

    onSelectSnap = (snap: number): void => {
        this.props.selectSnap(snap);
    };

    render(): JSX.Element {
        const {coinList, snapIndex, selectedCoin, selectedSnapNum} = this.props;
        const coinSnapshots: CoinSnapshot[] | undefined = snapIndex[selectedCoin.symbol_safe];

        return <React.Fragment>
            <StatusBar barStyle="dark-content"/>
            <Header/>
            <SafeAreaView style={{flex: 1}}>
                {
                    !_.isEmpty(coinList) && selectedCoin &&
                    <CoinMenu
	                    onSelectCoin={this.onSelectCoin}
	                    coinList={coinList}
                    />
                    || <Text>Loading CoinMenu...</Text>
                }
                {
                    !_.isEmpty(coinSnapshots) && selectedSnapNum &&
                    <SnapshotDetail
	                    onSelectSnap={this.onSelectSnap}
	                    icons={icons}
                        coinSnapshots={coinSnapshots}
                        selectedSnapNum={this.props.selectedSnapNum}
                        selectedCoin={this.props.selectedCoin}
                    />
                    || <LoadingSpinner/>
                }
            </SafeAreaView>
            <Footer/>
        </React.Fragment>;
    }
}

const mapStateToProps = ({
    CoinReducer: {coinList, selectedCoin},
    SnapReducer: {snapIndex, selectedSnapNum},
}: AppState) => ({coinList, selectedCoin, snapIndex, selectedSnapNum});

const mapDispatchToProps = dispatch => ({
    getListOfAllCoins: () => dispatch(getListOfAllCoins()),
    selectCoin: (coin: MetaCoin) => dispatch(selectCoin(coin)),
    getAllSnapsForCoin: (coin: string) => dispatch(getAllSnapsForCoin(coin)),
    selectSnap: (snap: number) => dispatch(selectSnap(snap)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SnapViewScreen);
