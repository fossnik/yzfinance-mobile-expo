export interface CoinSnapshot {
    ID: number
    dateCreated: string
    price: number
    change: number
    pChange: number
    marketCap: number
    volume: number
    volume24h: number
    totalVolume24h: number
    circulatingSupply: number
}

export interface SnapAction {
    type: string
    payload: {
        symbol_safe: string
        snapshots: CoinSnapshot[]
    }
}

export interface SnapState {
    snapIndex: SnapIndex
    selectedSnapNum: number
}

export interface SnapIndex {
    [key: string]: CoinSnapshot[]
}
