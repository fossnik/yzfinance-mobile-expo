import {API_URL} from '../../config';
import {GET_ALL_SNAPSHOTS_FOR_COIN, SELECT_SNAP} from './snapReducer';
import {SnapAction} from './types';

export const getAllSnapsForCoin = (symbol_safe: string): Promise<SnapAction> =>
    fetch(`${API_URL}/query_coin/${symbol_safe}`)
        .then(res => res.json().then(json => res.ok ? json : Promise.reject(json)))
        .then(res => Promise.resolve({
            type: GET_ALL_SNAPSHOTS_FOR_COIN,
            payload: {symbol_safe, snapshots: res.snapshots}
        }))
        .catch(err => Promise.reject({'Could not Load from API': err}));

export const selectSnap = (snap: number) => ({
    type: SELECT_SNAP,
    payload: snap,
});

