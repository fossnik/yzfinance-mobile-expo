import {SnapAction, SnapState} from './types';

export const GET_ALL_SNAPSHOTS_FOR_COIN = 'GET_ALL_SNAPSHOTS_FOR_COIN';
export const SELECT_SNAP = 'SELECT_SNAP';

const initialState: SnapState = {
    snapIndex: {},
    selectedSnapNum: 1,
};

export const SnapReducer = (
    state: SnapState = initialState,
    action: SnapAction,
) => {
    switch (action.type) {
        case GET_ALL_SNAPSHOTS_FOR_COIN:
            return {
                ...state,
                snapIndex: {
                    ...state.snapIndex,
                    [action.payload.symbol_safe]: action.payload.snapshots,
                }
            };

        case SELECT_SNAP:
            return {
                ...state,
                selectedSnapNum: action.payload,
            };

        default:
            return state;
    }
};
