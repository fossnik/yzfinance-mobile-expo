import {combineReducers} from 'redux';
import {CoinReducer} from './coin/coinReducer';
import {SnapReducer} from './snapshot/snapReducer';

export const rootReducer = combineReducers({
    CoinReducer,
    SnapReducer,
});

export type AppState = ReturnType<typeof rootReducer>;
