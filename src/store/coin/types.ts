export interface MetaCoin {
    name: string
    symbol_safe: string
    symbol_full: string
    symbol_icon_url: string
}

export interface CoinAction {
    type: string
    payload: MetaCoin[]
}

export interface CoinState {
    coinList: MetaCoin[]
    selectedCoin: MetaCoin
}
