import {CoinAction, CoinState} from './types';
import {truncateNames} from '../../helpers';

export const GET_LIST_OF_ALL_COINS = 'GET_LIST_OF_ALL_COINS';
export const SELECT_COIN = 'SELECT_COIN';

const initialState: CoinState = {
    coinList: [],
    selectedCoin: {
        name: 'Bitcoin',
        symbol_safe: 'btcusd',
        symbol_full: 'BTC-USD',
        symbol_icon_url: '',
    },
};

export const CoinReducer = (
    state: CoinState = initialState,
    action: CoinAction,
) => {
    switch (action.type) {
        case GET_LIST_OF_ALL_COINS:
            return {
                ...state,
                coinList: truncateNames(action.payload),
            };

        case SELECT_COIN:
            return {
                ...state,
                selectedCoin: action.payload,
            };

        default:
            return state;
    }
};

