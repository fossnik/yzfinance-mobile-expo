import {applyMiddleware, compose, createStore} from 'redux';
import {rootReducer as reducer} from './index';
import promise from 'redux-promise';

const middleware = [promise];

const composeEnhancers =
    window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] as typeof compose
    || compose;

const store = createStore(
    reducer,
    /* preloadedState, */
    composeEnhancers(
        applyMiddleware(...middleware)
    )
);

export default store;
